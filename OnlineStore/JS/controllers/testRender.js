import { data } from "./data.js";

export let testRender = () => {
  let renderData = data;
  let brandArr = [];
  let num = 0;
  let stopFindIndex = null;
  renderData.map((item) => {
    let index = 0;
    if (item.brand != stopFindIndex) {
      index = brandArr.findIndex((phone) => {
        console.log(++num);
        stopFindIndex = item.brand;
        return phone == item.brand;
      });
    }
    let containerContent = "";
    if (index == -1) {
      brandArr.push(item.brand);
      containerContent = `
    <div class="product-cover">
        <div class="product-brand-logo" style="background-color:${item.brandContainerBackground}">
            <img src=${item.brandLogoImg} alt="" />
        </div>
        <div id="${item.brand}-phone-container" class="product-container"></div>
    </div>
`;
    }
    let itemContent = `<div class="product-item">
    <img src=${item.img} alt="..." />
    <div class="product-content">
      <h5>${item.name}</h5>
      <div class="product-specification">
        <ul>
          <li>
            <span>RAM:</span>
            <span>${item.ram}</span>
          </li>
          <li>
            <span>ROM:</span>
            <span>${item.rom}</span>
          </li>
          <li>
            <span>Display:</span>
            <span>${item.display}</span>
          </li>
        </ul>
      </div>
      <div class="price flex-between-center">
        <span>From $${item.price}</span>
        <button>Buy</button>
      </div>
    </div>
  </div>`;
    document.getElementById("product-big-list").innerHTML += containerContent;
    document.getElementById(`${item.brand}-phone-container`).innerHTML +=
      itemContent;
  });
};
